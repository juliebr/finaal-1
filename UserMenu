import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;


public class UserMenu {
	
	private JFrame window;
	private JPanel menu = new JPanel();
	private JScrollPane instructionPage;
	private JPanel gameView = new JPanel();
	private JLabel player;
	private JLabel title;
	private JLabel difficulty;
	private JLabel instructionImage;
	private JPanel settings = new JPanel();
	private GridBagConstraints gbc = new GridBagConstraints();
	private JPanel cards;
	private CardLayout cardLay;
	private JPanel gameBoard;
	private gameController game;
	JButton nextLevel = new JButton("Next level");
	JButton retry = new JButton("Try again");
	
	private Difficulty currDifficulty = Difficulty.EASY;
	private GameType currGameType = GameType.SINGLE;
	
	//Add start button, if difficulty and player mode arnt selected print error	
    public static void main(String[] args) {
        UserMenu userMenu = new UserMenu();
        userMenu.createFrame();
        userMenu.buildInstructions();
        userMenu.addCards();
        userMenu.buildGameView();
        userMenu.buildUserMenu();
        userMenu.buildSettings();
        userMenu.window.setVisible(true);
    }
	
	
    public void createFrame() {
        window = new JFrame("WareHouse Boss");
        window.setSize(1000, 1000);
        window.setVisible(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        menu.setLayout(new GridBagLayout());
        settings.setLayout(new GridBagLayout());
    }
 
    public void addCards() {
        cardLay = new CardLayout();
        cards = new JPanel(cardLay);
        cards.add(menu, "Menu");
        cards.add(instructionPage, "Tutorial");
        cards.add(gameView, "GameView");
        cards.add(settings, "Settings");
        window.add(cards);
    }
	
    public void buildSettings() {
    	//Title of settings page
        settings.setBackground(Color.GRAY.darker());
        title = new JLabel("Warehouse Boss");
        title.setForeground(Color.WHITE.brighter());
        title.setFont(title.getFont().deriveFont((float) 96));
        gbc.gridx = 0;
        gbc.gridy = 0;
        settings.add(title, gbc);
        
        JLabel space = new JLabel("     ");
        gbc.gridy = 1;
        settings.add(space, gbc);
        
        JLabel space1 = new JLabel("     ");
        gbc.gridy = 2;
        settings.add(space1, gbc);
        
        JLabel space2= new JLabel("     ");
        gbc.gridy = 3;
        settings.add(space2, gbc);
        
        JLabel space3= new JLabel("     ");
        gbc.gridy = 4;
        settings.add(space3, gbc); 
        
        JLabel space4= new JLabel("     ");
        gbc.gridy = 5;
        settings.add(space4, gbc); 
        
        JLabel space5= new JLabel("     ");
        gbc.gridy = 6;
        settings.add(space5, gbc);
        
        JLabel space6= new JLabel("     ");
        gbc.gridy = 7;
        settings.add(space6, gbc);
        
        JLabel space7= new JLabel("     ");
        gbc.gridy = 8;
        settings.add(space7, gbc);
        
        JLabel space8= new JLabel("     ");
        gbc.gridy = 9;
        settings.add(space8, gbc);
        
        JLabel space9= new JLabel("     ");
        gbc.gridy = 10;
        settings.add(space9, gbc);
        
        JLabel space10= new JLabel("     ");
        gbc.gridy = 11;
        settings.add(space10, gbc);
        
        
        
        JLabel space23= new JLabel("     ");
        gbc.gridy = 24;
        settings.add(space23, gbc);
        
        JLabel space24= new JLabel("     ");
        gbc.gridy = 25;
        settings.add(space24, gbc);
        
        JLabel space25= new JLabel("     ");
        gbc.gridy = 26;
        settings.add(space25, gbc);
        
        JLabel space26= new JLabel("     ");
        gbc.gridy = 27;
        settings.add(space26, gbc);
        
        JLabel space27= new JLabel("     ");
        gbc.gridy = 28;
        settings.add(space27, gbc);
        
        //Title for Game settings
        settings.setBackground(Color.GRAY.darker());
        title = new JLabel("Game settings");
        title.setForeground(Color.WHITE.brighter());
        title.setFont(title.getFont().deriveFont((float) 64));
        gbc.gridy = 11;
        settings.add(title, gbc);
        
        //Sub-title for the player options
        player = new JLabel("Player settings", JLabel.CENTER);
        player.setForeground(Color.WHITE.brighter());
        player.setFont(player.getFont().deriveFont((float) 48));
        gbc.gridy = 12;
        settings.add(player, gbc);
        
        //Creates the single player button
        JRadioButton singleButton = new JRadioButton("Single");
        singleButton.setForeground(Color.WHITE);
        singleButton.setFont(singleButton.getFont().deriveFont((float) 32)); //Change font and size
        singleButton.setActionCommand("Single Player");
        singleButton.addActionListener(new ButtonClickListener());
        gbc.gridy = 13;
        settings.add(singleButton, gbc);
        
        //Creates the multi-player button
        JRadioButton multiButton = new JRadioButton("Multiplayer");
        multiButton.setForeground(Color.WHITE);
        multiButton.setFont(multiButton.getFont().deriveFont((float) 32));
        multiButton.setActionCommand("Multiplayer");
        multiButton.addActionListener(new ButtonClickListener());
        gbc.gridy = 14;
        settings.add(multiButton, gbc);
 
        //Groups player options together so only one can be selected
        Box playerBox = Box.createHorizontalBox();
        ButtonGroup playerGroup = new ButtonGroup();
        playerGroup.add(singleButton);
        playerGroup.add(multiButton);
        playerBox.add(singleButton);
        playerBox.add(multiButton);
        gbc.gridy = 15; //position of buttons
        settings.add(playerBox, gbc);
        
        //Difficulty label
        difficulty = new JLabel("Difficulty settings", JLabel.CENTER);
        difficulty.setForeground(Color.WHITE.brighter());
        difficulty.setFont(difficulty.getFont().deriveFont((float) 48));
        gbc.gridy = 16;
        settings.add(difficulty, gbc);
 
        //Create easy button option
        JRadioButton easyButton = new JRadioButton("Easy");
        easyButton.setForeground(Color.WHITE);
        easyButton.setFont(easyButton.getFont().deriveFont((float) 32));
        easyButton.setActionCommand("Easy");
        easyButton.addActionListener(new ButtonClickListener());
        gbc.gridy = 17;
        settings.add(easyButton, gbc);
 
        //Create medium button option
        JRadioButton mediumButton = new JRadioButton("Medium");
        mediumButton.setForeground(Color.WHITE);
        mediumButton.setFont(mediumButton.getFont().deriveFont((float) 32));
        mediumButton.setActionCommand("Medium");
        mediumButton.addActionListener(new ButtonClickListener());
        gbc.gridy = 18;
        settings.add(mediumButton, gbc);
 
        //Create hard button option
        JRadioButton hardButton = new JRadioButton("Hard");
        hardButton.setForeground(Color.WHITE);
        hardButton.setFont(hardButton.getFont().deriveFont((float) 32));
        hardButton.setActionCommand("Hard");
        hardButton.addActionListener(new ButtonClickListener());
        gbc.gridy = 19;
        settings.add(hardButton, gbc);
 
        //Group these buttons together so that only 1 option can be selected
        Box difficultyBox = Box.createHorizontalBox();
        ButtonGroup eGroup = new ButtonGroup();
        eGroup.add(easyButton);
        eGroup.add(mediumButton);
        eGroup.add(hardButton);
        difficultyBox.add(easyButton);
        difficultyBox.add(mediumButton);
        difficultyBox.add(hardButton);
        gbc.gridy = 20;
        settings.add(difficultyBox, gbc);
        
        //Return back to menu button
        JButton returnMenu = new JButton("Return");
        returnMenu.setForeground(Color.BLACK);
        returnMenu.setFont(returnMenu.getFont().deriveFont((float) 38));
        returnMenu.setActionCommand("Return");
        returnMenu.addActionListener(new ButtonClickListener());
        returnMenu.setPreferredSize(new Dimension(250, 75));
        gbc.gridy = 21;
        settings.add(returnMenu, gbc);
        
        JLabel space11= new JLabel("     ");
        gbc.gridy = 22;
        settings.add(space11, gbc);
        
        JLabel space12= new JLabel("     ");
        gbc.gridy = 23;
        settings.add(space12, gbc);
        
        JLabel space13= new JLabel("     ");
        gbc.gridy = 24;
        settings.add(space13, gbc);
        
        JLabel space14= new JLabel("     ");
        gbc.gridy = 25;
        settings.add(space14, gbc);
        
        JLabel space15 = new JLabel("     ");
        gbc.gridy = 26;
        settings.add(space15, gbc);
        
        JLabel space16= new JLabel("     ");
        gbc.gridy = 27;
        settings.add(space16, gbc);
        
        JLabel space17= new JLabel("     ");
        gbc.gridy = 28;
        settings.add(space17, gbc);
        
        JLabel space18= new JLabel("     ");
        gbc.gridy = 29;
        settings.add(space18, gbc);
        
        JLabel space19= new JLabel("     ");
        gbc.gridy = 30;
        settings.add(space19, gbc);
        
        JLabel space20= new JLabel("     ");
        gbc.gridy = 31;
        settings.add(space20, gbc);
        
        JLabel space21= new JLabel("     ");
        gbc.gridy = 32;
        settings.add(space21, gbc); 
        
        JLabel space22= new JLabel("     ");
        gbc.gridy = 33;
        settings.add(space22, gbc);
    }
	
    public void buildUserMenu() {
    	 menu.setBackground(Color.GRAY.darker());
         title = new JLabel("Warehouse Boss");
         title.setForeground(Color.WHITE.brighter());
         title.setFont(title.getFont().deriveFont((float) 96));
         gbc.gridx = 0;
         gbc.gridy = 0;
         menu.add(title, gbc);
         
         //Creates the button to go to Tutorial page
         JButton helpButton = new JButton("Tutorial");
         helpButton.setForeground(Color.BLACK);
         helpButton.setFont(helpButton.getFont().deriveFont((float) 48));
         helpButton.setActionCommand("Tutorial");
         helpButton.addActionListener(new ButtonClickListener());
         helpButton.setPreferredSize(new Dimension(400, 100));
  
         gbc.gridy = 12;
         menu.add(helpButton, gbc);
         
         //Creates the start button to start the game
         JButton startButton = new JButton("Start Game");
         startButton.setForeground(Color.BLACK);
         startButton.setFont(helpButton.getFont().deriveFont((float) 48));
         startButton.setActionCommand("Start");
         startButton.addActionListener(new ButtonClickListener());
         startButton.setPreferredSize(new Dimension(400, 100));
  
         gbc.gridy = 11;
         menu.add(startButton, gbc);
         
         //Creates the button to get to settings page
         JButton settingsButton = new JButton("Settings");
         settingsButton.setForeground(Color.BLACK);
         settingsButton.setFont(helpButton.getFont().deriveFont((float) 48));
         settingsButton.setActionCommand("Settings");
         settingsButton.addActionListener(new ButtonClickListener());
         settingsButton.setPreferredSize(new Dimension(400, 100));
  
         gbc.gridy = 13;
         menu.add(settingsButton, gbc);
         
         //Button which allows user to select level
         JButton readLevel = new JButton("Read Level");
         readLevel.setForeground(Color.BLACK);
         readLevel.setFont(helpButton.getFont().deriveFont((float) 48));
         readLevel.setActionCommand("ReadLevel");
         readLevel.addActionListener(new ButtonClickListener());
         readLevel.setPreferredSize(new Dimension(400, 100));
  
         gbc.gridy = 14;
         menu.add(readLevel, gbc);
 
    }
	
	
    public void buildInstructions() {
        JPanel addPanel = new JPanel(new BorderLayout());
        addPanel.setBackground(Color.GRAY.darker());
        instructionPage = new JScrollPane(addPanel);
        instructionPage.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        instructionPage.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        JLabel instructionTitle = new JLabel("WELCOME TO BOX PUZZLE!");
        instructionTitle.setForeground(Color.RED);
        instructionTitle.setFont(instructionTitle.getFont().deriveFont((float) 48));
        instructionTitle.setHorizontalAlignment(JLabel.CENTER);
        instructionTitle.setVerticalAlignment(JLabel.CENTER);
        addPanel.add(instructionTitle, BorderLayout.NORTH);
        
        JPanel buttons = new JPanel(new FlowLayout());
        
        JButton demoButton = new JButton("Demo");
        demoButton.setForeground(Color.BLACK);
        demoButton.setFont(demoButton.getFont().deriveFont((float) 24));
        demoButton.setActionCommand("Demo");
        demoButton.addActionListener(new ButtonClickListener());
        buttons.add(demoButton);
 
        JButton returnMenu = new JButton("Return");
        returnMenu.setForeground(Color.BLUE);
        returnMenu.setFont(returnMenu.getFont().deriveFont((float) 24));
        returnMenu.setActionCommand("Return");
        returnMenu.addActionListener(new ButtonClickListener());
        buttons.add(returnMenu, BorderLayout.SOUTH);
        
        addPanel.add(buttons,BorderLayout.SOUTH);
        //Loads instruction image into the instructions panel
        ImageIcon Icon = new ImageIcon("/Users/prasa/Desktop/Ass3/instructions.png");
        instructionImage = new JLabel(Icon);
        Image image = Icon.getImage();
        instructionImage.setSize(window.getHeight(), Icon.getIconHeight());
        Image newImg = image.getScaledInstance(instructionImage.getWidth(), instructionImage.getHeight(), Image.SCALE_SMOOTH);
        Icon = new ImageIcon(newImg);
        instructionImage = new JLabel(Icon);
        addPanel.add(instructionImage);
    }
   
	
	public void buildGameView() {
		
		gameView.setLayout(new BorderLayout());
		
		JPanel buttonPanel = new JPanel(new GridLayout(20,5,0,0 ));
		buttonPanel.setBackground(Color.WHITE.darker());
		
		//Retry button
		retry.setActionCommand("Retry");
		retry.addActionListener(new ButtonClickListener());
		buttonPanel.add(retry);
				
		//Next Level button
		nextLevel.setActionCommand("NextLevel");
		nextLevel.addActionListener(new ButtonClickListener());
		buttonPanel.add(nextLevel);
		
		//home button
		JButton homeButton = new JButton("Home");
		homeButton.setActionCommand("Home");
		homeButton.addActionListener(new ButtonClickListener());
		buttonPanel.add(homeButton);
		
		//undo button
		JButton undo = new JButton("Undo");
		undo.setActionCommand("Undo");
		undo.addActionListener(new ButtonClickListener());
		buttonPanel.add(undo);
		gameView.add(buttonPanel, BorderLayout.EAST);
		undo.setVisible(false);
		
		//left Panel
		JPanel leftPanel = new JPanel();
		leftPanel.setBackground(Color.WHITE.darker());
	    gameView.add(leftPanel, BorderLayout.WEST);
	    
	    //center gameBoard
	    gameBoard = new JPanel();
	    gameBoard.setBackground(Color.WHITE.darker());
	    gameView.add(gameBoard, BorderLayout.CENTER);
	    game = new GameController(gameBoard, window, undo);
	    
	    //for resizing
		window.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent evt) {
				leftPanel.setPreferredSize(new Dimension(window.getWidth()/7,window.getHeight()));
				buttonPanel.setPreferredSize(new Dimension(window.getWidth()/7,window.getHeight()));
				window.setVisible(true);
			}
		});
	}
	
	
	//Function which tells user what option has been selected
	private class ButtonClickListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();
			if(command.equals("Single Player")) {
				//Go to game with single player function
				currGameType = GameType.SINGLE;
			} else if (command.equals("Multiplayer")) {
				//Go to multiplayer game
				currGameType = GameType.MULTI;
			} else if (command.equals("Easy")) {
				currDifficulty = Difficulty.EASY;
			} else if (command.equals("Medium")) {
				currDifficulty = Difficulty.MEDIUM;
			} else if (command.equals("Hard")) {
				currDifficulty = Difficulty.HARD;
			} else if (command.equals("Tutorial")) {
				cardLay.show(cards, "Tutorial");
			} else if (command.equals("Return")) {
				cardLay.show(cards, "Menu");
			} else if (command.equals("Demo")) {
				nextLevel.setVisible(false);
				retry.setVisible(false);
                cardLay.show(cards, "GameView");
                game.tutorialGame();
            } else if (command.equals("Home")) {
				cardLay.show(cards, "Menu");
			} else if (command.equals("Start")) {
				nextLevel.setVisible(true);
				retry.setVisible(true);
				if(currDifficulty == Difficulty.NONE) {
					difficulty.setText("Please Select a Difficulty");
					difficulty.setForeground(Color.RED);
				}
				if(currGameType == GameType.NONE) {
					player.setText("Please Select a Game Type");
					player.setForeground(Color.RED);
				}
				if (currDifficulty != Difficulty.NONE && currGameType != GameType.NONE) {
					cardLay.show(cards, "GameView");
					game.playNewGame(currDifficulty, currGameType);
				}
			} else if (command.equals("Retry")) {
				game.retryGame();
			} else if (command.equals("NextLevel")) {
				game.playNewGame(currDifficulty, currGameType);
			} else if (command.equals("ReadLevel")) {
				nextLevel.setVisible(true);
				retry.setVisible(true);
				cardLay.show(cards, "GameView");
				game.importedGame();
			} else if (command.equals("Undo")) {
				game.Undo();
				gameBoard.requestFocus();
			} else if (command.equals("Settings")) {
	            cardLay.show(cards, "Settings");
	        }
				
		}  
	}
	
	
}